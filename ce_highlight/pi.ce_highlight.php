<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
ini_set('display_errors', '1');
/*
====================================================================================================
 Author: Aaron Waldon (Causing Effect)
 http://www.causingeffect.com
====================================================================================================
 This file must be placed in the /system/expressionengine/third_party/ce_highlight folder in your ExpressionEngine installation.
 package 		CE Highlight (EE2 Version)
 version 		Version 1.0
 copyright 		Copyright (c) 2010 Causing Effect, Aaron Waldon <aaron@causingeffect.com>
 Last Update	05 December 2010
----------------------------------------------------------------------------------------------------
 Purpose: Powerful image manipulation made easy
====================================================================================================

License: I guess this must be under GNU GPL, as Geshi is, and it uses it a lot.

*/

$plugin_info = array(
						'pi_name'			=> 'CE Highlight',
						'pi_version'		=> '1.0',
						'pi_author'			=> 'Aaron Waldon (Causing Effect)',
						'pi_author_url'		=> 'http://www.causingeffect.com/software/ee/ce_highlight',
						'pi_description'	=> 'Powerful image manipulation made easy.',
						'pi_usage'			=> Ce_highlight::usage()
					);


class Ce_highlight
{
	private $debug = FALSE;

	function __construct()
	{
		//EE super global
		$this->EE =& get_instance();

		//get debug param
		$this->debug = ( $this->EE->TMPL->fetch_param('debug') == 'yes' ) ? TRUE : FALSE;
	}

	/**
	 * Native PHP highlighter.
	 *
	 * @return mixed
	 */
	public function php()
	{
		$tagdata = trim($this->EE->TMPL->tagdata);

		//top colors param
		$type = $this->EE->TMPL->fetch_param('type');
		if ( $type == 'file' )
		{
			//determine the file path

			//return the highlighted file
			/*
			$temp = @highlight_file( $filename );
			if ( $temp !== FALSE )
			{
				return $temp;
			}
			else
			{
				return $this->EE->TMPL->no_results();
			}
			*/
			return $this->EE->TMPL->no_results();
		}
		else //highlight string
		{
			//if there is no tag data, there's not much we can do
			if ( $tagdata == '' )
			{
				return $this->EE->TMPL->no_results();
			}

			//include php tags if needed
			if ( substr( $tagdata, 0, 5) != '<?php' )
			{
				$tagdata = '<?php' . PHP_EOL . $tagdata . PHP_EOL . '?>';
			}

			//return the highlighted string
			return highlight_string( $tagdata, TRUE );
		}
	}

	private function include_geshi()
	{
		//---------- include GeSHI ----------
		$path = dirname( __FILE__ ) . '/libraries/geshi.php';
		$path = str_replace("\\", "/", $path );
		include_once( $path );
	}

	public function geshi()
	{
		$tagdata = $this->EE->TMPL->tagdata;

		//trim the tagdata?
		$trim = $this->EE->TMPL->fetch_param('trim');
		if ( $trim != 'no' )
		{
			$tagdata = trim( $tagdata );
		}

		//if there is no tag data, there's not much we can do
		if ( $tagdata == '' )
		{
			return $this->EE->TMPL->no_results();
		}

		//---------- include geshi ----------
		$this->include_geshi();

		//---------- determine language parameter ----------
		//language parameter
		$language = $this->EE->TMPL->fetch_param('language');
		if ( $language == '' )
		{
			$language = 'php';
		}

		//---------- decode tags ----------
		$decode = $this->EE->TMPL->fetch_param('encode_curly');
		if ( $decode == "yes" )
		{
			$tagdata = str_replace( array('{ exp', '{ !--'), array('{exp', '{!--'), $tagdata);
		}


		//---------- create the GeSHI instance ----------
		$geshi = new GeSHi( $tagdata, $language);

		//---------- determine other parameters ----------
		//enable classes parameter
		if ( $this->EE->TMPL->fetch_param('use_stylesheet') == "yes" )
		{
			$geshi->enable_classes();
		}

		//code container parameter
		$code_container = $this->EE->TMPL->fetch_param('code_container');
		if ( $code_container != '' )
		{
			$type = 'GESHI_HEADER_' . strtoupper( $code_container );
			$types = array('GESHI_HEADER_DIV', 'GESHI_HEADER_PRE', 'GESHI_HEADER_PRE_VALID', 'GESHI_HEADER_PRE_TABLE', 'GESHI_HEADER_NONE');
			if ( in_array( $type, $types ) )
			{
				$geshi->set_header_type( constant( $type ) );
			}
		}

		//set class parameter
		$class = $this->EE->TMPL->fetch_param('class');
		if ( $class != '' )
		{
			$geshi->set_overall_class( $class );
		}

		//set id parameter
		$id = $this->EE->TMPL->fetch_param('id');
		if ( $id != '' )
		{
			$temp = explode( "|", $id );
			if ( isset( $temp[0] ) )
			{
				$geshi->set_overall_id( $temp[0] );

				if ( isset( $temp[1] ) && ( $temp[1] == "yes" || $temp[1] == "y" || $temp[1] == "on" ) )
				{
					$geshi->enable_ids( TRUE );
				}
			}
		}
		//$geshi->enable_strict_mode( TRUE );

		//line numbers parameter
		$line_numbers = $this->EE->TMPL->fetch_param('line_numbers');
		if ( $line_numbers != '' )
		{
			$temp = explode( '|', $line_numbers );
			if ( $temp[0] == 'normal' )
			{
				$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
			}
			else if ( $temp[0] == 'fancy' )
			{
				if ( isset( $temp[1] ) && is_numeric( $temp[1] ) && $temp[1] > 0 )
				{
					$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS,  $temp[1]);
				}
				else
				{
					$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
				}
			}
			else
			{
				$geshi->enable_line_numbers(GESHI_NO_LINE_NUMBERS);
			}
		}

		//highlight_lines parameter
		$highlight_lines = $this->EE->TMPL->fetch_param('highlight_lines');
		if ( $highlight_lines != '' )
		{
			$temp = explode( '|', $highlight_lines );
			foreach( $temp as $index => $line )
			{
				if ( is_numeric( trim( $line ) ) )
				{
					$temp[$index] = trim($line);
				}
				else
				{
					unset( $temp[$index] );
				}
			}

			if ( count( $temp ) > 0 )
			{
				$geshi->highlight_lines_extra( $temp );
			}
		}

		/*
		//line styles parameter
		$line_styles = $this->EE->TMPL->fetch_param('line_styles');
		if ( $line_styles != '' )
		{
			$temp = explode( '|', $line_styles );
			if ( isset( $temp[0] ) )
			{
				if ( isset( $temp[1] ) )
				{
					$geshi->set_line_style( trim($temp[0]), trim($temp[1]) );
				}
				else
				{
					$geshi->set_line_style( trim($temp[0]) );
				}
			}
		}
		*/


		//line start number parameter
		$line_start_number = $this->EE->TMPL->fetch_param('line_start_number');
		if ( $line_start_number != '' && is_numeric( $line_start_number ) && $line_start_number >= 0 )
		{
			$geshi->start_line_numbers_at( round( $line_start_number ) );
		}

		//set character_set parameter
		$character_set = $this->EE->TMPL->fetch_param('character_set');
		if ( $character_set != '' )
		{
			$geshi->set_encoding( $character_set );
		}

		//set tab width parameter
		$tab_width = $this->EE->TMPL->fetch_param('tab_width');
		if ( $tab_width != '' && is_numeric( $tab_width ) && $tab_width >= 0 )
		{
			$geshi->set_tab_width( round( $tab_width ) );
		}

		//set header parameter
		$header = $this->EE->TMPL->fetch_param('header');
		if ( $header != '' )
		{
			$geshi->set_header_content( $header );
		}

		//set footer parameter
		$footer = $this->EE->TMPL->fetch_param('footer');
		if ( $footer != '' )
		{
			$geshi->set_footer_content( $footer );
		}

		//debug
		if ( $this->debug )
		{
			$error = $geshi->error();
			if ( $error )
			{
				if ( ! headers_sent() )
				{
					echo "<!-- CE Highlight - $error -->" . PHP_EOL;
				}
				$this->EE->TMPL->log_item( "<!-- CE Highlight - $error -->" );
			}
		}

		//disable keyword links
		$geshi->enable_keyword_links( FALSE );


		ob_start();
		$result = $geshi->parse_code();
		$code = ob_get_clean();

		//---------- decode tags ----------
		if ( $decode == "yes" )
		{
			$result = str_replace( array('{', '}'), array('&#123;','&#125;'), $result);
		}

		if ( ! $result )
		{
			if ( $this->debug )
			{
				if ( ! headers_sent() )
				{
					echo "<!-- CE Highlight - $code -->" . PHP_EOL;
				}
				$this->EE->TMPL->log_item( "<!-- CE Highlight - $code -->" );
			}

			return FALSE;
		}
		else
		{
			return $result;
		}
	}


	/**
	 * Returns the default styles for the specified languages.
	 *
	 * @return string
	 */
	public function geshi_default_stylesheet()
	{
		//include geshi
		$this->include_geshi();

		$styles = '';
		$errors = '';

		//set languages parameter
		$languages = $this->EE->TMPL->fetch_param('languages');
		if ( $languages != '' )
		{
			$temp = explode( '|', $languages );
			foreach( $temp as $language )
			{
				//geshi instance
				$geshi = new GeSHi( '', $language );
				//ech stylesheet
				$styles .= $geshi->get_stylesheet( FALSE );

				$errors .= $geshi->error();
			}
		}

		//debug
		if ( $this->debug )
		{
			if ( $errors )
			{
				if ( ! headers_sent() )
				{
					echo "<!-- CE Highlight - $errors -->" . PHP_EOL;
				}
				$this->EE->TMPL->log_item( "<!-- CE Highlight - $errors -->" );
			}
		}

		return $styles;
	}


	/**
	 * Plugin Usage
	 *
	 * @return string
	 */
	public static function usage()
	{
		ob_start();
?>

<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	} /* End of usage() function */

} /* End of class */
/* End of file pi.ce_highlight.php */
/* Location: ./system/expressionengine/third_party/ce_highlight/pi.ce_highlight.php */